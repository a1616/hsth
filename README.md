# Take home test for Hubstaff

## Task

Implement a solution to the problem below using TDD.

You are given an array of prices where prices[i] is the price of a given stock on the ith day,
and an integer fee representing a transaction fee.

Find the maximum profit you can achieve. You may complete as many transactions as you like
(i.e., buy one and sell one share of the stock multiple times) with the following restrictions:
After you sell your stock, you cannot buy stock on the next day (i.e., cooldown one day). You
may not engage in multiple transactions simultaneously (i.e., you must sell the stock before
you buy again).

## Building
This test is built using CMake 3.13.4 and Clang 7.0.1, which need to be installed in the system.
Also, git.

```
git clone https://gitlab.com/a1616/hsth.git 
cd hsth
mkdir build
cd build
cmake ..
cmake --build .
```

## Running

### Random data

Run it simply as `./hsth` and it will generate a random price list and an optimal
buy order for it(with a default fee of 10), and will dump it to stdout as json.

### Data from files

Run it as `./hsth <json-input>` and it will read the prices and fees from the json and
generate an optimal buy order, and will dump it to stdout as json.

### Tests
There is a test runner also built, `./hsth_tests`. Care must be taken to run it from the
repo /tests/data directory, or some tests will fail.

## Observations

- We are limited to having one share.
- On each day we can either: Do nothing, Sell, Buy, or Buy and Sell. More than that and we
either hit the 1 day cooldown or the share limit.
- Since the price is fixed, Buy and Sell is a losing strategy, so we can drop it.

## Implementation
To follow the TDD principles I always created tests before the code that was supposed to
exercise them, as I advanced in the implementation.

Although I started implementing an analytical solution, the edge cases for phyrric trades
(trades which lost money on the fee) were already complex, and I couldn't come up with a
formula which would take into account the buy-after-sell cooldown.

To bridge that last hurdle I implemented a pretty hackish annealer to test adjacent solutions.
I have it left at a pretty small number of iterations and the tuning is very rough, so I'm
sure it wouldn't be that difficult to generate a failing example. Although it's reasonably fast
it copies around a lot of order lists, so it's going to have issues with big listings.

Also this has been tested mostly on small listings. Since I had to generate my own data
I didn't really have the time to create a big price list to try, it's mostly toy examples.

I used catch2 and nlohmann's JSON as helper libraries

