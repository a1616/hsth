#include "evaluator.hpp"

#include <stdexcept>

float evaluate(const Prices& prices, const Orders& orders, int fee) {

    if(prices.size() != orders.size()) throw std::runtime_error("Price and order sizes are mismatched");
    if(!validate(orders)) throw std::runtime_error("Order book is not valid");

    float net_worth = 0.0f;
    for(int it = 0; it != prices.size(); it++) {
        auto order = orders.at(it);
        auto price = prices.at(it);

        switch(order) {
            case OrderTypes::buy:
                net_worth -= price;
                break;
            case OrderTypes::sell:
                net_worth += price;
                net_worth -= fee;
                break;
            default:
                break;
        }
    }

    return net_worth;
}

