#pragma once

#include "orders.hpp"
#include "prices.hpp"

#include <optional>

float evaluate(const Prices& prices, const Orders& orders, int fee);

