#include "generator.hpp"

#include "evaluator.hpp"
#include "mutator.hpp"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <set>
#include <vector>

Orders all_possible_trades(const Prices& prices)
{
    Orders orders{prices.size()};
    if(prices.size() < 2) return orders; // Edge case
   
    using set = std::set<size_t>;
    set peaks, valleys;
    
    size_t last = prices.size() - 1;
    bool ascending = false;
    for(size_t it = 0 ; it != last ; it++) {
        if(ascending) {
            if(prices[it] > prices[it+1]) {
                ascending = false;
                peaks.insert(it);
            }
        } else {
            if(prices[it] < prices[it+1]) {
                ascending = true;
                valleys.insert(it);
            }
        }
    }

    // Last element
    if(ascending) peaks.insert(last);

    for(auto index : peaks)   orders[index] = OrderTypes::sell;
    for(auto index : valleys) orders[index] = OrderTypes::buy;

    return orders;
}

bool remove_phyrric_trades(Orders& orders, const Prices& prices, int fee)
{
    assert(orders.size() == prices.size());

    bool did_something = false;
    size_t buy_price, buy_position;
    for(size_t it = 0 ; it != orders.size() ; it++) {
        auto p = prices[it];
        switch(orders[it]) {
            case OrderTypes::buy:
                buy_price = p;
                buy_position = it;
                break;
            case OrderTypes::sell:
                //Unprofitable, remove
                if( p - buy_price - fee < 0) {
                    orders[buy_position] = orders[it] = OrderTypes::wait;
                    did_something = true;
                }
                break;
            default:
                break;
        }
     
     }
    return did_something;
}

bool shift_illegals(Orders& orders, const Prices& prices)
{
    assert(orders.size() == prices.size());

    bool did_something = false;
    size_t sell_position;
    for(size_t it = 0 ; it != orders.size() ; it++) {
        switch(orders[it]) {
            case OrderTypes::sell:
                sell_position = it;
                break;
            case OrderTypes::buy:
                if(it - sell_position == 1) {
                    std::optional<float> sell, buy;
                    
                    //See if the sell can be moved
                    if(it > 1 && orders[it-2] == OrderTypes::wait) {
                        sell = prices[it-2] - prices[it-1];
                    }
                    //See if the buy can be moved
                    if(it + 1 != orders.size() && orders[it + 1] == OrderTypes::wait) {
                        buy = prices[it] - prices[it + 1];
                    }
                    if(!sell && !buy) continue; // Can't be fixed

                    did_something = true;
                    if(sell && buy) {
                        // Remove the worse trade
                        if(*sell < *buy) buy.reset();
                        else sell.reset();
                    }

                    if(sell) {
                        orders[it-2] = OrderTypes::sell;
                        orders[it-1] = OrderTypes::wait;
                    } else {
                        assert(buy && !sell);
                        orders[it+1] = OrderTypes::buy;
                        orders[it] = OrderTypes::wait;
                    }
                }
                break;
            default:
                break;
        }
    }
    return did_something;
}

bool remove_illegals(Orders& orders, const Prices& prices)
{
    assert(orders.size() == prices.size());

    bool did_something = false;
    size_t sell_position;
    for(size_t it = 0 ; it != orders.size() ; it++) {
        switch(orders[it]) {
            case OrderTypes::sell:
                sell_position = it;
                break;
            case OrderTypes::buy:
                if(it - sell_position == 1) {
                    did_something = true;
                    orders[it-1] = OrderTypes::wait;
                    orders[it] = OrderTypes::wait;
                }
                break;
            default:
                break;
        }
    }
    return did_something;
}


Orders generate_analytic_orders(const Prices& prices,int fee)
{
    auto orders = all_possible_trades(prices);

    bool did_something;
    do {
        did_something = false;
        bool result;
        do {
            result = remove_phyrric_trades(orders,prices,fee);
            if(result) did_something = true;
        } while(result);

        do {
            result = shift_illegals(orders,prices);
            if(result) did_something = true;
        } while(result);
        
        do {
            result = remove_illegals(orders,prices);
            if(result) did_something = true;
        } while(result);
    } while(did_something);
    
    return orders;
}

Orders generate_annealed_orders(const Prices& prices,int fee)
{
    auto best_orders = generate_analytic_orders(prices,fee);
    auto best_score = evaluate(prices,best_orders,fee);

    auto orders = best_orders;
    auto score = best_score;

    Mutator m;
    int rounds = 1000;
    int cool_rounds = rounds / 5;
    float starting_temp = std::accumulate(prices.begin(),prices.end(),0.0f)/prices.size() + fee * 10;

    // We let the annealer explore paths which are worse up to temperature, which gets lower over time
    for(size_t it = 0 ; it != rounds ; it++) {
        int rr = rounds - it - cool_rounds;
        float temperature = std::max(0.0f,rr * (starting_temp / rounds));
 
        auto new_orders = orders;
        m(new_orders);
        auto new_score = evaluate(prices,new_orders,fee);
        
        if(new_score > best_score) {
            best_orders = orders = new_orders;
            best_score = score = new_score;
        } else if(new_score + temperature > best_score) {
            orders = new_orders;
            score = new_score;
        } else {
            orders = best_orders;
            score = best_score;
        }
    }
   
    return orders;
}

Orders (*generate_orders)(const Prices& prices,int fee) = generate_annealed_orders;


