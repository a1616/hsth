#pragma once

#include "orders.hpp"
#include "prices.hpp"

extern Orders (*generate_orders)(const Prices& prices,int fee);

Orders generate_annealed_orders(const Prices& prices,int fee);
Orders generate_analytic_orders(const Prices& prices,int fee);

