#include "evaluator.hpp"
#include "generator.hpp"
#include "json.hpp"
#include "prices.hpp"

#include <fstream>
#include <iostream>

NLOHMANN_JSON_SERIALIZE_ENUM( OrderTypes, {
        {OrderTypes::wait, "wait"},
        {OrderTypes::buy, "buy"},
        {OrderTypes::sell, "sell"},
        })

int main(int argc, char** argv)
{
    Prices prices;
    auto default_size = 50;
    auto fee = 10;

    if(argc == 2) {
        {
            std::ifstream in(argv[1]);
            prices = load_prices(in);
        }
        {
            nlohmann::json j;
            std::ifstream in(argv[1]);
            in >> j;
            fee = j["fee"];
        }
    } else prices = make_random_prices(default_size);

    auto orders = generate_orders(prices,fee);

    auto result = evaluate(prices,orders,fee);
   
    nlohmann::json j;

    j["prices"] = prices;
    j["orders"] = orders;
    j["fee"] = fee;
    j["result"] = result;

    std::cout << j.dump(4) << std::endl;
    return 0;
}

