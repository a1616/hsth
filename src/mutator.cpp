#include "mutator.hpp"

#include <optional>

Mutator::Mutator(size_t seed)
{
    if(!seed) {
        std::random_device seeder;
        m_rng.seed(seeder());
    } else {
        m_rng.seed(seed);
    }
}

void Mutator::operator()(Orders& orders)
{
    auto pick = m_chooser(m_rng);
    if(pick < 96) shift(orders);
    else if(pick < 98) add(orders);
    else remove(orders);
}

size_t Mutator::ruint(size_t a,size_t b)
{
    return std::uniform_int_distribution<size_t>{a,b}(m_rng);
}

void Mutator::add(Orders& orders)
{
    auto size = orders.size();
    if(size < 2) return;
    auto shift = ruint(0, size - 1);

    std::optional<size_t> last_sell;
    size_t start,end;
    bool found = false;
    for(size_t base_it = 0 ; base_it != size && !found; base_it++) {
        auto it = (shift + base_it) % size;

        switch(orders[it]) {
            case OrderTypes::buy:
                // We first find the start of a free block
                if(!last_sell) continue;
                
                //maybe free space at the end and beggining, choose
                if(*last_sell > it) {
                    bool back_possible = (size - *last_sell) > 3;
                    bool front_possible = it > 2; // No previous sell, so no need for wait day

                    bool do_front;
                    // Not enough space at either end
                    if(!back_possible && !front_possible) continue;

                    found = true;
                    if(back_possible && front_possible) do_front = ruint(0,1);
                    else do_front = front_possible;

                    if(do_front) {
                        start = 0;
                        end = it-2;
                    } else {
                        start = *last_sell+2;
                        end = size-1;
                    }
                    break;
                }

                // Enough room for another add
                if(it - *last_sell > 4) {
                    start = *last_sell+2;
                    end = it-2;
                    found = true;
                }
                break;
            case OrderTypes::sell:
                last_sell = it;
                break;
            default:
                break;
        }
    }
    // Empty Order book
    if(!last_sell) {
        start = 0;
        end = size-1;
        found = true;
    }

    if(!found) return; // No room for more orders
    
    size_t new_buy,new_sell;
    new_buy= ruint(start,end);
    do{
        new_sell = ruint(start,end);
    } while(new_sell==new_buy);
    if(new_sell < new_buy) std::swap(new_sell,new_buy);

    //Finally
    orders[new_sell] = OrderTypes::sell;
    orders[new_buy] = OrderTypes::buy;
}

void Mutator::remove(Orders& orders)
{
    auto size = orders.size();
    auto shift = ruint(0, size - 1);

    std::optional<size_t> last_buy;
    for(size_t base_it = 0 ; base_it != size ; base_it++) {
        auto it = (shift + base_it) % size;

        switch(orders[it]) {
            case OrderTypes::sell:
                // We first find the start of a free block
                if(!last_buy) continue;

                //remove order
                orders[*last_buy] = orders[it] = OrderTypes::wait;
                return;
            case OrderTypes::buy:
                last_buy = it;
                break;
            default:
                break;
        }
    }
}

void Mutator::shift(Orders& orders)
{
    auto ruint = [this](size_t a,size_t b){ return std::uniform_int_distribution<size_t>{a,b}(m_rng); };
    auto size = orders.size();
    auto shift = ruint(0, size - 1);

    for(size_t base_it = 0 ; base_it != size ; base_it++) {
        auto it = (shift + base_it) % size;

        switch(orders[it]) {
            case OrderTypes::buy:
            case OrderTypes::sell:
                {
                    bool up_possible = (it < size -1) && (orders[it+1] == OrderTypes::wait);
                    
                    //Need to check that we don't add buy too fast after sell
                    if(it < size -2 && orders[it+2] != OrderTypes::wait) up_possible = false;                  
                    
                   
                    bool down_possible = (it > 0) && (orders[it-1] == OrderTypes::wait);

                    //Need to check that we don't add buy too fast after sell
                    if(it > 1 && orders[it-2] != OrderTypes::wait) down_possible = false;                  
                    
                    if(!up_possible && !down_possible) continue;
                    if(up_possible && down_possible) {
                        if(ruint(0,1)) up_possible = false;
                        else down_possible = false;
                    }

                    if(up_possible) orders[it+1] = orders[it];
                    else orders[it-1] = orders[it];
                    orders[it] = OrderTypes::wait;
                    return;
                }
                break;
            default:
                break;
        }
    }
}

