#pragma once

#include "orders.hpp"

#include <random>


class Mutator {
    public:
        Mutator(size_t seed = 0);
        void operator()(Orders& orders);
    private:
        size_t ruint(size_t a,size_t b);
        
        void add(Orders& orders);
        void remove(Orders& orders);
        void shift(Orders& orders);

        std::mt19937 m_rng;
        std::uniform_int_distribution<> m_chooser{0,100};
};
