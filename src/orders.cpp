#include "orders.hpp"

Orders make_orders(size_t size) {
    return Orders{size,OrderTypes::wait};
}

bool validate_implementation(const Orders& orders,bool ignore_fast_sells) {
    bool held = false;
    size_t last_sold = -1UL;

    for(size_t it = 0 ; it != orders.size() ; ++it) {
        auto order = orders.at(it);
        switch(order) {
            case OrderTypes::buy:
                {
                    if(held) return false;
                    bool first_day = it == 0;
                    if(!ignore_fast_sells && !first_day && it - 1 == last_sold) return false;
                    held = true;
                }
                break;
            case OrderTypes::sell:
                if(!held) return false;
                held = false;
                last_sold = it;
                break;
            default:
                break;
        }
    }
    return true;
}

bool validate(const Orders& orders) {
    return validate_implementation(orders,false);
}

bool validate_except_fast_trades(const Orders& orders) {
    return validate_implementation(orders,true);
}

