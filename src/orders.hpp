#pragma once

#include <vector>

enum class OrderTypes{wait,buy,sell};

using Orders = std::vector<OrderTypes>;

Orders make_orders(size_t size);

bool validate(const Orders& orders);
bool validate_except_fast_trades(const Orders& orders);

