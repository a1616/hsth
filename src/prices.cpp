#include "prices.hpp"

#include "json.hpp"

#include <random>


Prices make_random_prices(size_t size)
{
    std::random_device seeder;
    std::mt19937 rng{seeder()};

    using ndist = std::normal_distribution<float>;

    const auto daily_change = 10.0f;
    ndist gen{0.0f,daily_change};

    auto current_value = ndist{500.0f,100.0f}(rng);
    Prices values;

    while(size--) {
        current_value += gen(rng);
        current_value = std::max(0.0f,current_value);
        values.push_back(current_value);
    }

    return values;
}

Prices load_prices(std::istream& in)
{
    nlohmann::json j;
    in >> j;

    Prices p;

    for (auto& el : j["prices"]) {
        p.push_back(el.get<float>());
    }
    return p;
}

void save_prices(std::ostream& out,const Prices& prices)
{
    nlohmann::json j;
    j["prices"] = prices;

    out << j;    
}

