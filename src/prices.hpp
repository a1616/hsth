#pragma once

#include <vector>
#include <iostream>

using Prices = std::vector<float>;

Prices make_random_prices(size_t size);

Prices load_prices(std::istream& in);
void save_prices(std::ostream& out,const Prices& prices);

