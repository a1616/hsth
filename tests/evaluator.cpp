#include "catch_amalgamated.hpp"

#include "../src/evaluator.hpp"

TEST_CASE("evaluator") {
    const auto size = 10;
    auto prices = make_random_prices(size);
    auto orders = make_orders(size);

    const auto fee = 10;

    SECTION("Evaluate empty order book") {
        auto result = evaluate(prices,orders,fee);
        CHECK(result == 0.0f);
    }

    SECTION("Evaluate single buy") {
        orders[0] = OrderTypes::buy;
        auto result = evaluate(prices,orders,fee);
        CHECK(result < 0.0f);
    }
 
    for(auto& price : prices) price = 100.0f;
    prices[0] = 50.0f;

    SECTION("Evaluate buy and sell") {
        orders[0] = OrderTypes::buy;
        orders[1] = OrderTypes::sell;
        CHECK(evaluate(prices,orders,fee) == 40.0f);
        orders[5] = OrderTypes::buy;
        orders[6] = OrderTypes::sell;
        CHECK(evaluate(prices,orders,fee) == 30.0f);
    }
}

