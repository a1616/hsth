#include "catch_amalgamated.hpp"

#include "../src/generator.hpp"

TEST_CASE("generator") {
    auto fee = 0;
    SECTION("Empty") {
        auto prices = Prices{};
        auto orders = Orders{};
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Single element") {
        auto prices = Prices{1.0f};
        auto orders = Orders{OrderTypes::wait};
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Basic buy low sell high") {
        auto prices = Prices{10.0f,15.0f,8.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 1] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Check that we sell at the end") {
        auto prices = Prices{10.0f,15.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 1] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Check that we don't buy at the start if the price is going down") {
        auto prices = Prices{13.0f,10.0f,15.0f};
        auto orders = make_orders(prices.size());
        orders[ 1] = OrderTypes::buy; orders[ 2] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("No trades at all") {
        auto prices = Prices{15.0f,10.0f};
        auto orders = make_orders(prices.size());
        CHECK(orders == generate_orders(prices,fee));
    }
    
    SECTION("Fee makes the trade not worth it") {
        fee = 6;
        auto prices = Prices{10.0f,15.0f};
        auto orders = make_orders(prices.size());
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Two phyrric trades which can be merged into a profitable one.") {
        fee = 6;
        auto prices = Prices{10.0f,15.0f,14.0f,13.0f,18.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }
    
    SECTION("Two phyrric trades which cannot be merged into a profitable one.") {
        fee = 6;
        auto prices = Prices{10.0f,11.0f,7.0f,5.0f,6.0f};
        auto orders = make_orders(prices.size());
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("One phyrric trade which if merged with the following one will make it worse") {
        fee = 6;
        auto prices = Prices{10.0f,11.0f,7.0f,5.0f,12.0f};
        auto orders = make_orders(prices.size());
        orders[ 3] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("One phyrric trade which leaves money in the table if completely removed") {
        fee = 6;
        auto prices = Prices{10.0f,20.0f,19.0f,18.0f,21.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    {
        auto prices = Prices{10.0f,15.0f,20.0f,20.0f,25.0f,22.0f,18.0f,15.0f,19.0f,21.0f, 14.0f,8.0f,5.0f,6.0f,7.0f,8.0f};
        
        SECTION("More complex buy pattern") {
            auto orders = make_orders(prices.size());
            orders[ 0] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
            orders[ 7] = OrderTypes::buy; orders[ 9] = OrderTypes::sell;
            orders[12] = OrderTypes::buy; orders[15] = OrderTypes::sell;
            CHECK(orders == generate_orders(prices,fee));
        }

        SECTION("More complex and fee makes some trades not worth it") {
            fee = 5;
            auto orders = make_orders(prices.size());
            orders[ 0] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
            orders[ 7] = OrderTypes::buy; orders[ 9] = OrderTypes::sell;
            CHECK(orders == generate_orders(prices,fee));
        }
    }

    SECTION("Profitable but invalid trade") {
        auto prices = Prices{15.0f,20.0f,10.0f,15.0f};
        auto orders = generate_orders(prices,fee);
        CHECK(prices.size() == orders.size());
        CAPTURE(orders);
        CHECK(validate(orders));
    }

    SECTION("Invalid trade which can be swapped to a less profitable, valid one, moving buy forward") {
        fee = 0;
        auto prices = Prices{10.0f,20.0f,10.0f,12.0f,20.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 1] = OrderTypes::sell;
        orders[ 3] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Invalid trade which can be swapped to a less profitable, valid one,moving sell backward") {
        fee = 0;
        auto prices = Prices{10.0f,19.0f,20.0f,10.0f,20.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 1] = OrderTypes::sell;
        orders[ 3] = OrderTypes::buy; orders[ 4] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }

    SECTION("Invalid trades which wouldn't be invalid if unprofitable trades were removed") {
        fee = 5;
        auto prices = Prices{10.0f,12.0f,11.0f,20.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 3] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }
    SECTION("Invalid trades which becomes valid but unprofitable if fixed") {
        fee = 5;
        auto prices = Prices{10.0f,20.0f,10.0f,13.0f,16.0f};
        auto orders = make_orders(prices.size());
        orders[ 0] = OrderTypes::buy; orders[ 1] = OrderTypes::sell;
        CHECK(orders == generate_orders(prices,fee));
    }
}

TEST_CASE("Trade generator extra tests") {
    // Not a great test but useful to fuzz the generator
    size_t fee = 5;
    SECTION("Check for validity random price listings") {
        for(int it = 0 ; it != 100 ; it++) {
            auto prices = make_random_prices(1000);
            CAPTURE(prices);
            auto orders = generate_orders(prices,fee);
            CAPTURE(orders);
            REQUIRE(validate_except_fast_trades(orders));
        }
    }

}
