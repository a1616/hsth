#include "catch_amalgamated.hpp"

#include "../src/mutator.hpp"

TEST_CASE("mutator") {
    
    SECTION("Mutate null order book and check validity") {
        Orders orders{0};
        Mutator m{1};
        auto iterations = 1000;
        for(int i = 0; i != iterations ; i++) m(orders);
        CAPTURE(orders);
        CHECK(validate(orders));
    }

    SECTION("Mutate very short order book and ensure it stays valid") {
        Orders orders{2};
        Mutator m{1};
        auto iterations = 1000;
        for(int i = 0; i != iterations ; i++) m(orders);
        CAPTURE(orders);
        CHECK(validate(orders));
    }

    SECTION("Mutate empty order book and ensure it stays valid") {
        Orders orders{30};
        Mutator m{1};
        auto iterations = 100000;
        for(int i = 0; i != iterations ; i++) m(orders);
        CAPTURE(orders);
        CHECK(validate(orders));
    }


    SECTION("Mutate really big order book and ensure it stays valid") {
        Orders orders{10000};
        Mutator m{1};
        auto iterations = 10;
        for(int i = 0; i != iterations ; i++) m(orders);
        CAPTURE(orders);
        CHECK(validate(orders));
    }


}

