#include "catch_amalgamated.hpp"

#include "../src/orders.hpp"

TEST_CASE("orders") {
    const auto size = 10;
    auto orders = make_orders(size);

    SECTION("Empty book") {
        // Empty book should be all wait, and thus valid
        CHECK(validate(orders) == true);
    }

    SECTION("Buy and sell") {
        orders[1] = OrderTypes::buy;
        CHECK(validate(orders) == true);
        orders[2] = OrderTypes::sell;
        CHECK(validate(orders) == true);
    }

    SECTION("Sell before buy")  {
        orders[1] = OrderTypes::sell;
        CHECK(validate(orders) == false);
    }

    SECTION("Double buy") {
        orders[1] = OrderTypes::buy;
        orders[2] = OrderTypes::buy;
        CHECK(validate(orders) == false);
    }

    SECTION("Double sell") {
        orders[1] = OrderTypes::buy;
        orders[2] = OrderTypes::sell;
        orders[3] = OrderTypes::sell;
        CHECK(validate(orders) == false);
    }

    SECTION("Buy too early after sell") {
        orders[1] = OrderTypes::buy;
        orders[2] = OrderTypes::sell;
        orders[3] = OrderTypes::buy;
        CHECK(validate(orders) == false);
    }

    SECTION("Test edge case of buying at day 0") {
        orders[0] = OrderTypes::buy;
        CHECK(validate(orders) == true);
    }
}

