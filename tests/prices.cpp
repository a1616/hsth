#include "catch_amalgamated.hpp"

#include "../src/prices.hpp"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <sstream>

TEST_CASE("prices") {
    const auto size = 10;
    auto prices = make_random_prices(size);

    CHECK(prices.size() == size);
    // Seems my compiler doesn't yet have none_of
    CHECK(std::all_of(prices.begin(), prices.end(), [](float v) { return !std::isnan(v); }));
    CHECK(std::all_of(prices.begin(), prices.end(), [](float v) { return v > 0.0f; }));

    auto load_save_load = [](auto name){
        std::ifstream file(name);
        auto prices = load_prices(file);
        std::stringstream save;

        save_prices(save,prices);
        auto prices2 = load_prices(save);
        return std::pair{ prices,prices2 };
    };

    SECTION("Empty file") {
        auto comparison_prices = Prices{};
        auto [prices, saved_prices] = load_save_load("empty.json");
        CHECK(prices == comparison_prices);
        CHECK(saved_prices == comparison_prices);
    }

    SECTION("One element") {
        auto comparison_prices = Prices{10.0f};
        auto [prices, saved_prices] = load_save_load("one.json");
        CHECK(prices == comparison_prices);
        CHECK(saved_prices == comparison_prices);
    }
    
    SECTION("Two elements") {
        auto comparison_prices = Prices{10.0f,10.0f};
        auto [prices, saved_prices] = load_save_load("two.json");
        CHECK(prices == comparison_prices);
        CHECK(saved_prices == comparison_prices);
    }
}


